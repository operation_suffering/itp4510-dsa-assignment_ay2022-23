import java.util.*;

public class OrderSystem {
    private static Scanner sc;
    private static LinkedList orders;
    private static int nextGuestID = 9000;

    public static void main(String[] args) throws InvalidInputException {
        sc = new Scanner(System.in);
        orders = new LinkedList();
        int memberID;

        boolean isExit = false;

        while (!isExit) {

            System.out.print("Please input your member ID [input 0 for guest]:");

            try {
                memberID = sc.nextInt();
            } catch (InvalidInputException e) {
                System.out.println("Invalid input! Please input again.");
                sc.next();
                continue;
            } catch (InputMismatchException e) {
                System.out.println("Input Error");
                sc.next();
                continue;
            }
            if (memberID == 9999) {
                adminFunc();
            } else if (memberID < 0) {
                System.out.println("Have a nice day!!!");
                isExit = true;
            } else if ((memberID > 8000 && memberID < 9000) || memberID == 0) {
                inputOrder(memberID);
            } else {
                System.out.println("Invalid input! Please input again.");
            }
        }
        sc.close();
    }


    public static void inputOrder(int memberID) throws InvalidInputException {
        int priority = 0;

        if (memberID == 0) {
            memberID = nextGuestID++;
        }
        System.out.println("----------------- Food Menu ----------------");
        System.out.println("Set A : Chicken Salad");
        System.out.println("Set B : Grilled Ribeye Steak");
        System.out.println("Set C : Angel Hair Pasta with Shrimp");
        System.out.println("Set D : Grilled Fish and Potatoes");
        System.out.println("--------------------------------------------");
        System.out.print("Select food: ");
        try {
            String food = sc.next().toUpperCase();

            if (!food.equals("A") && !food.equals("B") && !food.equals("C") && !food.equals("D")) {
                throw new InvalidInputException("");
            }
            if (memberID > 8000 && memberID < 8200) {
                priority = 1;
            } else if (memberID < 9000 && memberID >= 8200) {
                priority = 2;
            } else if (memberID >= 9000) {
                priority = 3;
            }

            FoodOrder order = new FoodOrder(memberID, food, priority);
            orders.add(order);

        } catch (InvalidInputException e) {
            System.out.println("Invalid input! Please input again.");
        }
    }


    public static void adminFunc() throws NoSuchElementException {
        int input;
        try{
            System.out.println("-------------- Admin Function --------------");
            System.out.println("1 : Print order list");
            System.out.println("2 : Remove order");
            System.out.println("3 : Back to menu");

            System.out.print("Enter your choice: ");
            input = sc.nextInt();
        if (input == 1) {
            if (orders.isEmpty()) {
                throw new NoSuchElementException("");
            }
            System.out.println(orders.toString());
        } else if (input == 2) {
            System.out.print("Enter Member ID:");
            int memberID = sc.nextInt();
            orders.remove(memberID);
        }
        } catch (EmptyListException e) {
            System.out.println("None of order");
        } catch (InputMismatchException e) {
            System.out.println("Input Error");
            sc.next();
        } catch (NoSuchElementException e){
            System.out.println("List is empty");
        }
    }
}