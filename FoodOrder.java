public class FoodOrder {
    private int memberID;
    private String foodOrder; // A, B, C, or D
    private int priority;

    //constructor
    public FoodOrder(int memberID, String foodOrder, int priority) {
        this.memberID = memberID;
        this.foodOrder = foodOrder;
        this.priority = priority;

    }

    public int getMemberID() {
        return memberID;
    }

    //provide methods getter, setter, toString ....
    public void setMemberID(int memberID) {
        this.memberID = memberID;
    }

    public String getFoodOrder() {
        return foodOrder;
    }

    public void setFoodOrder(String foodOrder) {

        this.foodOrder = foodOrder;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority){
        this.priority = priority;
    }

    public String toString() {
        return "[ MemberID: " + memberID + " ordered Set " + foodOrder + " with priority " + priority + " ]\n";
    }
}