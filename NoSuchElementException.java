public class NoSuchElementException extends RuntimeException{
    public NoSuchElementException(String msg) {
        super(msg);
        System.out.print(msg);
    }
}
